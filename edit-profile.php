<?php
/**
*This page is dashboard page. user access this page only after login
*/
session_start();

//check if user logged in
if(empty($_SESSION['user_id']))
{
	echo '<script>window.location = "index.php"</script>';
}

//including database class file
require_once('libraries/Db_curd.php');

$db = new Db_curd;
$data = array(
	'first_name',
	'middle_name',
	'last_name',
	'email',
	'gender',
	'mobile',
	'address',
	'country_id',
	'state_id',
	'city_id',
	'zip_code',
	'image',
	);

$row = $db->select('users', $data, ['user_id' => $_SESSION['user_id']]);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Dashborad</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
</head>
<body>
	<div class="container">
   		<div class="row profile">
			<!--sidebar start-->
			<?php require_once('sidebar.php'); ?>
			<!--end sidebar-->
			<div class="col-md-9 profile-content">
				<form method="post" action="edit.php" enctype="multipart/form-data">
				   <div class="row">
						<div class="col-md-4 form-group">
							<label for="first_name">First Name <span class="required">*</span></label>
							<input type="text" placeholder="Enter First Name Here.." class="form-control" name="first_name" id="first_name" maxlength="20" value="<?php echo (! empty($row['first_name'])) ? $row['first_name'] : ''; ?>" required>
							<span class="error-msg"><?php echo (! empty($_SESSION['first_name_error'])) ? $_SESSION['first_name_error'] : '';?></span>
						</div>
						<div class="col-md-4 form-group">
							<label for="middle_name">Middle Name</label>
							<input type="text" placeholder="Enter Middle Name Here.." value="<?php echo (! empty($row['middle_name'])) ? $row['middle_name'] : ''; ?>" class="form-control" name="middle_name" id="middle_name" maxlength="20">
						</div>
						<div class="col-md-4 form-group">
							<label for="last_name">Last Name</label>
							<input type="text" placeholder="Enter Last Name Here.." value="<?php echo (! empty($row['last_name'])) ? $row['last_name'] : ''; ?>" class="form-control" name="last_name" id="last_name" maxlength="20">
						</div>
					</div>
					<div class="form-group">
						<label for="email">Email Address <span class="required">*</span></label>
						<input type="email" placeholder="Enter Email Address Here.." class="form-control" name="email" id="email" maxlength="40" value="<?php echo (! empty($row['email'])) ? $row['email'] : ''; ?>" required disabled>
						<span class="error-msg"><?php echo (! empty($_SESSION['email_error'])) ? $_SESSION['email_error'] : '';?></span>
					</div>
					<div class="form-group">
						<label for="phone">Phone Number <span class="required">*</span></label>
						<input type="text" placeholder="Enter Phone Number Here.." class="form-control" name="phone" maxlength="10" minlength="10" id="phone" value="<?php echo (! empty($row['mobile'])) ? $row['mobile'] : ''; ?>" required>
						<span class="error-msg"><?php echo (! empty($_SESSION['phone_error'])) ? $_SESSION['phone_error'] : '';?></span>
					</div>
					<div class="form-group">
						<label>Gender <span class="required">*</span></label>
						<input type="radio" name="gender" value="1" <?php echo ($row['gender'] == 1) ? 'checked' : ''; ?>>Male
						<input type="radio" name="gender" value="2" <?php echo ($row['gender'] == 2) ? 'checked' : ''; ?>>Female
						<input type="radio" name="gender" value="3" <?php echo ($row['gender'] == 3) ? 'checked' : ''; ?>>Other<br>
						<span class="error-msg"><?php echo (! empty($_SESSION['gender_error'])) ? $_SESSION['gender_error'] : '';?></span>
					</div>					
					<div class="form-group">
						<label for="address">Address <span class="required">*</span></label>
						<textarea placeholder="Enter Address Here.." rows="3" class="form-control" id="address" maxlength="100" name="address" required>
							<?php echo (! empty($row['address'])) ? $row['address'] : ''; ?>
						</textarea>
						<span class="error-msg"><?php echo (! empty($_SESSION['address_error'])) ? $_SESSION['address_error'] : '';?></span>
					</div>	
					<div class="row">
						<div class="col-md-3 form-group">
							<label for="country">Country <span class="required">*</span></label>
							<select class="form-control" name="country" id="country" onchange="state_list(this.value)" required>
								<option value="">select country</option>
								<?php
									$data = array(
										'id',
										'name'
										);
									$result = $db->multiple_rows('countries', $data);

									if($result)
									{
										//showing result of each row
										foreach($result as $country)
										{
								?>
											<option value="<?php echo $country['id']; ?>" <?php echo ($row['country_id'] == $country['id']) ? 'selected' : ''; ?>><?php echo $country['name']; ?></option>
								<?php
										}
									}
									else
									{
										echo '<option value="">Country not available</option>';
									}
								?>
							</select>
							<span class="error-msg"><?php echo (! empty($_SESSION['country_error'])) ? $_SESSION['country_error'] : '';?></span>
						</div>
						<div class="col-md-3 form-group">
							<label for="state">State <span class="required">*</span></label>
							<select class="form-control" id="state" name="state" onchange="city_list(this.value)" required>
								<option value="">Select country first</option>
							</select>
							<span class="error-msg"><?php echo (! empty($_SESSION['state_error'])) ? $_SESSION['state_error'] : '';?></span>
						</div>	
						<div class="col-md-3 form-group">
							<label for="city">City <span class="required">*</span></label>
							<select id="city" name="city" class="form-control">
								<option value="">Select state first</option>
							</select>
							<span class="error-msg"><?php echo (! empty($_SESSION['city_error'])) ? $_SESSION['city_error'] : '';?></span>
						</div>	
						<div class="col-md-3 form-group">
							<label for="zip">Zip Code<span class="required">*</span></label>
							<input type="text" placeholder="Enter Zip Code Here.." class="form-control" value="<?php echo (! empty($row['zip_code'])) ? $row['zip_code'] : ''; ?>" maxlength="6" id="zip" name="zip" required>
							<span class="error-msg"><?php echo (! empty($_SESSION['zip_error'])) ? $_SESSION['zip_error'] : '';?></span>
						</div>		
					</div>
					<!--<div class="form-group">
						<label for="image">Profile Photo</label>
						<input type="file" class="form-control" value="<?php echo (! empty($row['image'])) ? $row['image'] : ''; ?>" name="image" id="image" accept=".jpg, .png, .jpeg">
						<span class="error-msg"><?php echo (! empty($_SESSION['image_error'])) ? $_SESSION['image_error'] : '';?></span>
					</div>	-->								
					<button type="Submit" class="btn btn-lg btn-info" name="update">Update</button>
				</form>	        
			</div>
    	</div>
	</div>

	<!--including js files-->
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/common.js"></script>
</body>
</html>