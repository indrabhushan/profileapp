<?php
/**
*This page is open when registartion form getting submit
*/
session_start();
//Including  form validation library
require_once('libraries/Form_validation.php');
require_once('libraries/Db_curd.php');

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
	//Create object of form validation
	$form_validation = new Form_validation;
	$db = new Db_curd;

	$first_name   = $form_validation->check_first_name($_POST['first_name']);
	$middle_name  = $form_validation->check_alphabet_only($_POST['middle_name'], 'middle_name_error', 'Please enter letters only');
	$last_name    = $form_validation->check_alphabet_only($_POST['last_name'], 'last_name_error', 'Please enter letters only');
	$phone        = $form_validation->check_phone($_POST['phone']);
	$gender       = $form_validation->check_gender($_POST['gender']);
	$address      = $form_validation->check_address($_POST['address']);
	$country      = $form_validation->check_country_state($_POST['country'], 'country_error', 'Please select country');
	$state        = $form_validation->check_country_state($_POST['state'], 'state_error', 'Please select state');
	$city         = $form_validation->check_country_state($_POST['city'], 'city_error', 'Please select city');
	$zip          = $form_validation->check_zip_code($_POST['zip']);

	$res = $first_name  & $phone & $gender & $address & $state & $country & $zip & $city;
	//Check validation error then redirect to register page
	if(($res && $middle_name && $last_name) 
		|| ($res && empty($_POST['middle_name']) && empty($_POST['last_name'])))
	{
		$date = date('Y-m-d H:i:s');
		$data = array(
				'first_name'  => $first_name,
				'middle_name' => $middle_name,
				'last_name'   => $last_name,
				'gender'      => (int) $gender,
				'mobile'      => $phone,
				'address'     => $address,
				'country_id'  => (int) $country,
				'state_id'    => (int) $state,
				'city_id'     => (int) $city,
				'zip_code'    => $zip,
				'updated_by'  => $first_name,
				'updated_date'=> $date,
			);
		
		if($db->update('users', $data, ['user_id' => $_SESSION['user_id']]))
		{
			echo '<script>alert("Update successfully");</script>';
			echo '<script>window.location = "dashboard.php";</script>';
		}
		else
		{
			echo '<script>alert("Internal srver error");</script>';
			echo '<script>window.location = "edit-profile.php"</script>';
		}
	}
	else
	{
		echo '<script>window.location = "edit-profile.php"</script>';
	}
}
?>