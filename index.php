<?php
session_start();
//if user already logged in then redirect to dashboard page
if( ! empty($_SESSION['user_id']))
{
	echo '<script>window.location = "dashboard.php"</script>';
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
</head>
<body>
	<div id="fullscreen_bg" class="fullscreen_bg"/>
		<div class="container">
			<?php
			require_once('session-message.php');
			?>

			<form class="form-signin" method="post" action="login.php">
				<h1 class="form-signin-heading text-muted">Sign In</h1>
				<input type="email" class="form-control" placeholder="Email address" name="email" value="<?php echo isset($_SESSION['post_data']['email']) ? $_SESSION['post_data']['email'] : ''; ?>" required autofocus>
				<div class="back-white"><?php echo (! empty($_SESSION['email_error'])) ? $_SESSION['email_error'] : '';?></div>
				<input type="password" class="form-control" placeholder="Password" name="password" required>
				<div class="back-white"><?php echo (! empty($_SESSION['password_error'])) ? $_SESSION['password_error'] : '';?></div>
				<button class="btn btn-lg btn-primary btn-block" type="submit" name="login">
					Login
				</button>
				<div style="background: white;">
					New user?<a href="registration/register.php">Sign Up</a>
				</div>
			</form>
		</div>
	</div>
</body>
</html>