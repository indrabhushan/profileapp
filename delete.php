<?php 
/**
*This page is user delete. user access this page only after login
*/
session_start();

//check if user logged in
if(empty($_SESSION['user_id']))
{
	echo '<script>window.location = "index.php"</script>';
}

require_once('libraries/Db_curd.php');

if( ! empty($_GET['delete_id']))
{
	$db = new Db_curd;

	if($db->delete('users', ['user_id' => $_GET['delete_id']]))
	{
		echo '<script>alert("User deleted successfully");</script>';
		echo '<script>window.location = "user-listing.php"</script>';
	}
	else
	{
		echo '<script>alert("User deleted failed");</script>';
		echo '<script>window.location = "user-listing.php"</script>';
	}
}

?>