<div class="col-md-3">
	<div class="profile-sidebar">
		<div class="profile-usertitle">
		    <img src="<?php echo ( ! empty($_SESSION['image'])) ? 'assets/images/'.$_SESSION['image'] : ''; ?>" class="img-circle" width="100px" alt="img">
			<div class="profile-usertitle-name">
				<?php echo ( ! empty($_SESSION['name'])) ? $_SESSION['name'] : ''; ?>
			</div>
		</div>
		<!-- SIDEBAR MENU -->
		<div class="profile-usermenu">
			<ul class="nav">
				<li class="active">
					<a href="dashboard.php">
						<i class="glyphicon glyphicon-home"></i>
						Home 
					</a>
				</li>
				<li>
					<a href="edit-profile.php">
						<i class="glyphicon glyphicon-edit"></i>
						Edit Profile 
					</a>
				</li>
				<li>
					<a href="user-listing.php">
						<i class="glyphicon glyphicon-list-alt"></i>
						Users 
					</a>
				</li>
				<li>
					<a href="logout.php">
						<i class="glyphicon glyphicon-off"></i>
						Logout 
					</a>
				</li>
			</ul>
		</div>
		<!-- END MENU -->
	</div>
</div>