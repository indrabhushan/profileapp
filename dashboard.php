<?php
/**
*This page is dashboard page. user access this page only after login
*/
session_start();

//check if user logged in
if(empty($_SESSION['user_id']))
{
	echo '<script>window.location = "index.php"</script>';
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Dashborad</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
</head>
<body>
	<div class="container">
   		<div class="row profile">
			<!--sidebar start-->
			<?php require_once('sidebar.php'); ?>
			<!--end sidebar-->
			<div class="col-md-9 profile-content">
			   <h3>Welcome to Dashboard</h3>        
			</div>
    	</div>
	</div>
</body>
</html>