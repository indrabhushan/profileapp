<?php
/**
*This page is user listing page. user access this page only after login
*/
session_start();

//check if user logged in
if(empty($_SESSION['user_id']))
{
	echo '<script>window.location = "index.php"</script>';
}

require_once('libraries/Db_curd.php');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Dashborad</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
</head>
<body>
	<div class="container">
   		<div class="row profile">
			<!--sidebar start-->
			<?php require_once('sidebar.php'); ?>
			<!--end sidebar-->
			<div class="col-md-9 profile-content">
		   		<table class="table">
				    <thead>
				      <tr>
				        <th>Firstname</th>
				        <th>Email</th>
				        <th>Action</th>
				      </tr>
				    </thead>
				    <tbody>
				    <?php
				    	$db = new Db_curd;
				    	$data = array(
				    		'user_id',
				    		'first_name',
				    		'email'
				    		);
				    	$result = $db->multiple_rows('users', $data);

				    	foreach($result as $rows)
				    	{
				    ?>
				      <tr>
				        <td><?php echo ( ! empty($rows['first_name'])) ? $rows['first_name'] : ''; ?></td>
				        <td><?php echo ( ! empty($rows['email'])) ? $rows['email'] : ''; ?></td>
				        <td>
				        <?php 
				        	if($rows['user_id'] != $_SESSION['user_id'])
				        	{
				        ?>
				        	<a href="details.php?id=<?php echo $rows['user_id']; ?>">View Details</a>
				        	/<a href="delete.php?delete_id=<?php echo $rows['user_id']; ?>">Delete User</a>
				        <?php 
							}
						?>
						</td>
				      </tr>
				    <?php
				    	}
				    ?>
				    </tbody>
			  	</table>        
			</div>
    	</div>
	</div>
</body>
</html>