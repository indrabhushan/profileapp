<?php
if ($_SESSION['error_msg']) 
{
	?>
	<div class="alert alert-danger"><?php echo $_SESSION['error_msg'];?></div>
<?php
} 
elseif ($_SESSION['success_msg']) 
{
?>
	<div class="alert alert-success"><?php echo $_SESSION['success_msg'];?></div>
<?php
}
?>