<?php
/**
*This page is load after clicking on login button
*/
session_start();

require_once('libraries/Form_validation.php');
require_once('libraries/Db_curd.php');

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$form_validation = new Form_validation;
	$db = new Db_curd;

	$email    = $form_validation->check_email($_POST['email']);
	$password = $form_validation->check_password($_POST['password']);

	if( ! $email || ! $password)
	{
		//setting all posted data
		$_SESSION['post_data'] = $_POST;
		//redirect to login page
		header('location:index.php');
	}
	else
	{
		$data = array(
			'user_id',
			'first_name',
			'image',
			'password',
			);

		$result = $db->select('users', $data, ['email' => $email]);

		if($result)
		{
			if(password_verify($password, $result['password']))
			{
				//unset($_SESSION);
				$_SESSION = ['user_id' => $result['user_id'], 'name' => $result['first_name'], 'image' => $result['image']];
				echo '<script>window.location = "dashboard.php"</script>';
			}
			else
			{
				//setting all posted data
				$_SESSION['post_data'] = $_POST;
				$_SESSION['password_error'] = 'Please enter correct password';
				echo '<script>window.location = "index.php"</script>';
			}
		}
		else
		{
			//setting all posted data
			$_SESSION['post_data'] = $_POST;
			$_SESSION['email_error'] = 'Please enter correct email';
			echo '<script>window.location = "index.php"</script>';
		}
	}
}

/**
*@name check_email
*@desc this function is for checking empty and valid email or not
*@param $email submitted email
*@return $email
*/
function check_email($email)
{
	if(empty($email))
	{
		$_SESSION['email_error'] = 'Please enter email';
		return VALIDATION_ERROR;
	}
	else
	{
		$email = test_input($email);

		if ( ! filter_var($email, FILTER_VALIDATE_EMAIL)) 
	    {
	      $_SESSION['email_error'] = 'Please enter valid email address';
	      return VALIDATION_ERROR;
	    }
	    else
	    {
    		unset($_SESSION['email_error']);
    		return $email;
	    }
	}
}

/**
*@name check_empty_field
*@description this is common function for checking empty field or not
*@param $field_name which field you want to check, $message error message to show, $message_key which key want to set message
*@return $field_name value of that field
*/
function check_empty_field($field_name, $message, $message_key)
{
	if(empty($field_name))
	{
		$_SESSION[$message_key] = $message;
		return VALIDATION_ERROR;
	}
	else
	{
		unset($_SESSION[$message_key]);
		return test_input($field_name);
	}
}

/**
*@function test_input
*@description This function is for trimming extra white spaces and html special character
*@param $data form input data
*@return $data
*/
function test_input( $data )
{
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);

	return $data;
}
?>