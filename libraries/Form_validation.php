<?php
/**
*This file is for server side validation of registration form
*/

/**
*This class is for server side validation of registration
*@version PHP 7.0
*@author Indrabhushan Yadav
*@category Form validation
*/
class Form_validation {

	/**
	*@function test_input
	*@description This function is for trimming extra white spaces and html special character
	*@param $data form input data
	*@return $data
	*/
	private function test_input($data)
	{
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);

		return $data;
	}

	/**
	*@name check_first_name
	*@description This function is used for validating first name only alphabet allowed
	*@param $first_name
	*@return boolean, $first_name
	*/
	public function check_first_name($first_name)
	{
		if(empty($first_name))
		{
			$_SESSION['first_name_error'] = 'Please enter first name';
			return FALSE;
		}
		else
		{
			$first_name = $this->test_input($first_name);

			if (!preg_match("/^[a-zA-Z]*$/",$first_name)) 
			{
  				$_SESSION['first_name_error'] = 'Please enter letters only';
  				return FALSE;
			}

			unset($_SESSION['first_name_error']);
			return $first_name;
		}
	}

	/**
	*@name check_alphabet_only
	*@description This function is used for validating only aplhabet field
	*@param $field_value, $message_key, $message
	*@return boolean, $field_value
	*/
	public function check_alphabet_only($field_value, $message_key, $message)
	{
		$field_value = $this->test_input($field_value);

		if (!preg_match("/^[a-zA-Z]*$/",$field_value)) 
		{
			$_SESSION[$message_key] = $message;
			return FALSE;
		}

		unset($_SESSION[$message_key]);
		return $field_value;
	}

	/**
	*@name check_email
	*@desc this function is for checking empty and valid email or not
	*@param $email submitted email
	*@return $email
	*/
	public function check_email($email)
	{
		if(empty($email))
		{
			$_SESSION['email_error'] = 'Please enter email';
			return FALSE;
		}
		else
		{
			$email = $this->test_input($email);

			if ( ! filter_var($email, FILTER_VALIDATE_EMAIL)) 
		    {
		      $_SESSION['email_error'] = 'Please enter valid email address';
		      return FALSE;
		    }

    		unset($_SESSION['email_error']);
    		return $email;
		}
	}

	/**
	*@name check_password
	*@description This is for the validate password field empty and in between 6 to 15 character
	*@param $password
	*@return boolean, $password
	*/
	public function check_password($password)
	{
		if(empty($password))
		{
			$_SESSION['password_error'] = 'Please enter password';
			return FALSE;
		}
		else
		{
			$password    = $this->test_input($password);
			$pass_length = strlen($password);

			if($pass_length < 6 || $pass_length > 15)
			{
				$_SESSION['password_error'] = 'Please enter password in between 6 to 15 character';
				return FALSE;
			}
			else
			{
				unset($_SESSION['password_error']);
				return $password;
			}
		}
	}

	/**
	*@name check_cnf_password
	*@description This function is for check confirm password empty or in between 6 to 15 chracter and 
	*equal to the password
	*@param $password, $cnf_password
	*@return boolean, $cnf_password
	*/
	public function check_cnf_password($password, $cnf_password)
	{
		if(empty($cnf_password))
		{
			$_SESSION['cnf_password_error'] = 'Please enter confirm password';
			return FALSE;
		}
		else
		{
			$cnf_password     = $this->test_input($cnf_password);
			$cnf_pass_length = strlen($cnf_password);


			if($cnf_pass_length < 6 || $cnf_pass_length > 15)
			{
				$_SESSION['cnf_password_error'] = 'Please enter confirm password in between 6 to 15 character';
				return FALSE;
			}
			else
			{
				if($password != $cnf_password)
				{
					$_SESSION['cnf_password_error'] = 'Please enter same password again';
					return FALSE;
				}
				else
				{
					unset($_SESSION['cnf_password_error']);
					return $cnf_password;
				}
			}
		}
	}

	/**
	*@name check_phone
	*@description This function is for checking phone number is empty or not and numeric value
	*@param $phone
	*@return $phone, boolean
	*/
	public function check_phone($phone)
	{
		if(empty($phone))
		{
			$_SESSION['phone_error'] = 'Please enter phone number';
			return FALSE;
		}
		else
		{
			$phone = $this->test_input($phone);

			if( ! is_numeric($phone))
			{
				$_SESSION['phone_error'] = 'Please enter valid phone number';
				return FALSE;
			}
			else
			{
				unset($_SESSION['phone_error']);
				return $phone;
			}
		}
	}

	/**
	*@name check_gender
	*@description This function is for validate gender value
	*@param $gender
	*@return boolean, $gender
	*/
	public function check_gender($gender)
	{
		if(empty($gender))
		{
			$_SESSION['gender_error'] = 'Please select gender';
			return FALSE;
		}
		else
		{
			$gender     = $this->test_input($gender);
			$gender_len = strlen($gender);

			if($gender_len != 1 || ! is_numeric($gender))
			{
				$_SESSION['gender_error'] = 'Please select gender';
				return FALSE;
			}
			else
			{
				unset($_SESSION['gender_error']);
				return $gender;
			}
		}
	}
	/**
	*@name check_address
	*@description This function is for checking address is empty or not
	*@param $address
	*@return boolean, $address
	*/
	public function check_address($address)
	{
		if(empty($address))
		{
			$_SESSION['address_error'] = 'Please enter address';
			return FALSE;
		}
		else
		{
			unset($_SESSION['address_error']);
			return $this->test_input($address);
		}
	}

	/**
	*@name check_country_state
	*@description This function is for the checking empty field and numeric value
	*@param $field_value, $message_key, $message
	*@return boolean, $field_value
	*/
	public function check_country_state($field_value, $message_key, $message)
	{
		if(empty($field_value))
		{
			$_SESSION[$message_key] = $message;
			return FALSE;
		}
		else
		{
			$field_value = $this->test_input($field_value);
			
			if( ! is_numeric($field_value))
			{
				$_SESSION[$message_key] = $message;
				return FALSE;
			}
			else
			{
				unset($_SESSION[$message_key]);
				return $field_value;
			}	
		}
	}

	/**
	*@name check_city
	*@description This function is for validate city
	*@param $city
	*@return $city, boolean
	*/
	public function check_city($city)
	{
		$city = $this->test_input($city);

		if( ! is_numeric($field_value))
		{
			$_SESSION['city_error'] = 'Please select city';
		}

		unset($_SESSION['city_error']);
		return $city;
	}

	/**
	*@name check_zip_code
	*@description This function is for check empty and numeric value for the zip code field
	*@param $zip
	*@return boolean, $zip
	*/
	public function check_zip_code($zip)
	{
		if(empty($zip))
		{
			$_SESSION['zip_error'] = 'Please enter zip code';
			return FALSE;
		}
		else
		{
			$zip = $this->test_input($zip);

			if( ! is_numeric($zip))
			{
				$_SESSION['zip_error'] = 'Please enter only numbers';
				return FALSE;
			}
			else
			{
				unset($_SESSION['zip_error']);
				return $zip;
			}
		}
	}

	/**
	*@name image_upload
	*@description this function is for the uploading image
	*@param $file_name, $tmp_name, $file_size, $image_type
	*@return $file_name
	*/
	function image_upload($image)
	{
		if(empty($image))
		{
			return TRUE;
		}

		$target_dir = "../assets/images/";
		$target_file = $target_dir . basename($image['name']);
		$upload_ok  = 1;
		
		// Check file size
		if ($image['size'] > 2000000) 
		{
		    $_SESSION['image_error'] = 'Only 2 mb image allowed';
		    $upload_ok = 0;
		}

		/*$image['type'] = strtolower($image['type']);
		// Allow certain file formats
		if(    $image['type'] != 'image/jpg' 
			|| $image['type'] != 'image/png' 
			|| $image['type'] != 'image/jpeg'
		   ) 
		{
		    $_SESSION['image_error'] = 'Sorry, only JPG, JPEG, PNG files are allowed.';
		    $upload_ok = 0;
		}*/
		// Check if $upload_ok is set to 0 by an error
		if ($upload_ok == 0) 
		{
		    return FALSE;
		} 
		else
		{
		    if (move_uploaded_file($image['tmp_name'], $target_file)) 
		    {
		    	unset($_SESSION['image_error']);
		        return $image['name'];
		    } 
		    else 
		    {
		    	$_SESSION['image_error'] = 'Sorry, there was an error uploading your file.';
		        return FALSE;
		    }
		}
	}
}
?>