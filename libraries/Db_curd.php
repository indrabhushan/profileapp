<?php
/**
*This page is for creating database connection and insert, update, delete operation in database
*/
//Including contants file
require_once('constants.php');

class Db_curd {

	private $conn = '';

	public function __construct()
	{
		//Create connection
		$this->conn = new mysqli(SERVER_NAME, USER_NAME, PASSWORD, DATABASE);

		if($conn->connect_error)
		{
			die('Connection failed:'. $conn->connect_error);
		}
	}

	public function insert($table_name, $data)
	{
		foreach(array_keys($data) as $key)
		{
			$fields[] = "`$key`";
			$values[] = '"'.$this->conn->real_escape_string($data[$key]).'"';
		} 

		$fields = implode(',', $fields);
		$values = implode(',', $values);
		$sql = " INSERT INTO $table_name ($fields) VALUES ($values)";

		if($this->conn->query($sql))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public function select($table_name, $data, $where)
	{
		$data = implode(',', $data);
		$field = array_keys($where);
		$field = $field[0];
		$sql = "SELECT $data FROM $table_name WHERE $field = '$where[$field]'";
		$result = $this->conn->query($sql);

		if($result->num_rows > 0)
		{
			return $result->fetch_assoc();
		}
		else
		{
			return FALSE;
		}
	}

	public function multiple_rows($table_name, $data)
	{
		$data = implode(',', $data);
		$sql = "SELECT $data FROM $table_name";
		$result = $this->conn->query($sql);

		if($result->num_rows > 0)
		{
			while($row = $result->fetch_assoc())
			{
				$rows[] = $row;
			}
			return $rows;
		}
		else
		{
			return FALSE;
		}
	}

	public function update($table_name, $data, $where)
	{
		foreach(array_keys($data) as $key)
		{
			$values[] = "`$key` = '".$this->conn->real_escape_string($data[$key])."'";
		} 

		$field = array_keys($where);
		$field = $field[0];
		$values = implode(',', $values);
		$sql   =" UPDATE $table_name SET $values WHERE $field = $where[$field]";

		if($this->conn->query($sql))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		} 
	}

	public function delete($table_name, $where)
	{
		$field = array_keys($where);
		$field = $field[0];
		$sql   = "DELETE FROM $table_name WHERE $field = $where[$field]";

		if($this->conn->query($sql))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}
?>