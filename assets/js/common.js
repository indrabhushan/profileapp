/**
*@name state_list
*@description this function is for send the ajax request and get all the states according
*to that country
*@param country_id selected country id
*/
function state_list(country_id)
{
	if(country_id)
	{
		$.ajax({
			url: 'ajax-data.php',
			type: 'post',
			data : 'country_id='+country_id,
			success: function(response)
			{
				$('#state').html(response);
				$('#city').html('<option value="">Select state first</option>');
			}
		});
	}
	else
	{
		$('#state').html('<option value="">Select country first</option>');
		$('#city').html('<option value="">Select state first</option>');
	}
}

/**
*@name city_list
*@description this function is for send ajax request and get all the cities list according
*to that state
*@param state_id selected state id
*/
function city_list(state_id)
{
	if(state_id)
	{
		$.ajax({
			url: 'ajax-data.php',
			type: 'post',
			data : 'state_id='+state_id,
			success: function(response)
			{
				$('#city').html(response);
			}
		});
	}
	else
	{
		$('#city').html('<option value="">Select state first</option>');
	}
}

/**
*@name check_email
*@description This function is for send AJAX request to ajax-check-email.php and check
*email already exist in database or not and empty field or valid email adddress
*@param email
*/
function check_email(email)
{
	var email_error = document.getElementById('email_error');
	var email_obj   = document.getElementById('email');

	if( ! email)
	{
		email_error.innerHTML = 'Please enter email';
		email_obj.style.borderColor = 'red';
		return false;
	}
	else
	{
		var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		
		if( ! email.match(pattern))
		{
			email_error.innerHTML = 'Please enter valid email';
			email_obj.style.borderColor = 'red';
			return false;
		}
		else
		{
			var xhttp       = new XMLHttpRequest();    

			xhttp.onreadystatechange = function() 
			{
				if(this.readyState == 4 && this.status == 200)
				{
					var obj = JSON.parse(this.responseText);
					
					if(obj.code === 200)
					{
						email_error.innerHTML = obj.message;
						email_obj.style.borderColor = 'red';
						return false;
					}
					else
					{
						email_error.innerHTML = '';
						email_obj.style.borderColor = 'green';
						return true;
					}
				}
			}

			xhttp.open('POST', 'ajax-check-email.php', true);
			xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xhttp.setRequestHeader("X-Requested-With", "xmlhttprequest");
			xhttp.send('email='+email);
		}
	}
}

/**
*@name check_first_name
*@description This function is for checking first name empty or letters only
*@param first_name
*
*/
function check_first_name(first_name)
{
	var first_name       = first_name.trim();
	var first_name_obj   = document.getElementById('first_name');
	var first_name_error = document.getElementById('first_name_error');

	if( ! first_name)
	{
		first_name_error.innerHTML = 'Please enter first name';
		first_name_obj.style.borderColor ='red';
		return false;
	}
	else
	{
		var letters = /^[A-Za-z]+$/;

		if( ! first_name.match(letters))
		{
			first_name_error.innerHTML = 'Please enter letters only';
			first_name_obj.style.borderColor ='red';
			return false;
		}
		else
		{
			first_name_error.innerHTML = '';
			first_name_obj.style.borderColor ='green';
			return true;
		}
	}
}

/**
*@name check_middle_name
*@description This function is for checking middle name is alphabet or not
*@param middle_name
*/
function check_middle_name(middle_name)
{
	var middle_name       = middle_name.trim();
	var middle_name_obj   = document.getElementById('middle_name');
	var middle_name_error = document.getElementById('middle_name_error');

	if( ! middle_name)
	{
		return true;
	}
	else
	{
		var letters = /^[A-Za-z]+$/;

		if( ! middle_name.match(letters))
		{
			middle_name_error.innerHTML = 'Please enter letters only';
			middle_name_obj.style.borderColor = 'red';
			return false;
		}
		else
		{
			middle_name_error.innerHTML = '';
			middle_name_obj.style.borderColor = 'green';
			return true;
		}
	}
}

/**
*@name check_last_name
*@description This function is for checking last name is alphabet or not
*@param last_name
*/
function check_last_name(last_name)
{
	var last_name       = last_name.trim();
	var last_name_obj   = document.getElementById('last_name');
	var last_name_error = document.getElementById('last_name_error');

	if( ! last_name)
	{
		return true;
	}
	else
	{
		var letters = /^[A-Za-z]+$/;

		if( ! last_name.match(letters))
		{
			last_name_error.innerHTML = 'Please enter letters only';
			last_name_obj.style.borderColor = 'red';
			return false;
		}
		else
		{
			last_name_error.innerHTML = '';
			last_name_obj.style.borderColor = 'green';
			return true;
		}
	}
}

/**
*@function check_password
*@description This function is for checking password empty field and in bitween
* 6 to 15 chracters
*@param password
*@return boolean
*/
function check_password(password)
{
	var password       = password.trim();
	var password_obj   = document.getElementById('password');
	var password_error = document.getElementById('password_error');

	if( ! password)
	{
		password_error.innerHTML = 'Please enter password';
		password_obj.style.borderColor = 'red';
		return false;
	}
	else
	{
		if(password.length < 6 || password.length > 15)
		{
			password_error.innerHTML = 'Please enter password in between 6 to 15 character';
			password_obj.style.borderColor = 'red';
			return false;
		}
		else
		{
			password_error.innerHTML = '';
			password_obj.style.borderColor = 'green';
			return true;
		}
	}
}

/**
*@function check_cnf_password
*@description This function is for checking confirm password empty field and in bitween
* 6 to 15 chracters and equal to the password
*@param cnf_password
*@return boolean
*/
function check_cnf_password(cnf_password)
{
	var cnf_password       = cnf_password.trim();
	var cnf_password_obj   = document.getElementById('cnf_password');
	var cnf_password_error = document.getElementById('cnf_password_error');
	var password           = document.getElementById('password').value;

	if( ! cnf_password)
	{
		cnf_password_error.innerHTML = 'Please enter confirm password';
		cnf_password_obj.style.borderColor = 'red';
		return false;
	}
	else
	{
		if(cnf_password.length < 6 || cnf_password.length > 15)
		{
			cnf_password_error.innerHTML = 'Please enter confirm password in between 6 to 15 character';
			cnf_password_obj.style.borderColor = 'red';
			return false;
		}
		else
		{
			if(cnf_password != password)
			{
				cnf_password_error.innerHTML = 'Please enter same password again';
				cnf_password_obj.style.borderColor = 'red';
				return false;
			}
			else
			{
				cnf_password_error.innerHTML = '';
				cnf_password_obj.style.borderColor = 'green';
				return true;
			}
		}
	}
}

/**
*@name check_phone
*@description This function is for checking phone number empty and only digits
*@param phone
*@return boolean
*/
function check_phone(phone)
{
	var phone       = phone.trim();
	var phone_obj   = document.getElementById('phone');
	var phone_error = document.getElementById('phone_error');

	if( ! phone)
	{
		phone_error.innerHTML = 'Please enter phone number';
		phone_obj.style.borderColor = 'red';
		return false;
	}
	else
	{
		var digits = /^[0-9]+$/;
		if( ! phone.match(digits))
		{
			phone_error.innerHTML = 'Please enter valid phone number';
			phone_obj.style.borderColor = 'red';
			return false;
		}
		else
		{
			if(phone.length != 10)
			{
				phone_error.innerHTML = 'Please enter 10 digit phone number';
				phone_obj.style.borderColor = 'red';
				return false;
			}
			else
			{
				phone_error.innerHTML = '';
				phone_obj.style.borderColor = 'green';
				return true;
			}
		}
	}
}

/*function check_gender(gender)
{
	var gender       = gender.trim();
	var gender_error = document.getElementById('gender_error');

	if( ! gender)
	{
		gender_error.innerHTML = 'Please select gender';
		return false;
	}
	else
	{
		if(gender != 1 || gender != 2 gender != 3)
		{
			gender_error.innerHTML = 'Please select gender';
			return false;
		}
		else
		{
			gender_error.innerHTML = '';
			return true;
		}
	}
}*/

/**
*@name check_address
*@description This function is for checking address field is empty or not
*@param address
*@return boolean
*/
function check_address(address)
{
	var address       = address.trim();
	var address_obj   = document.getElementById('address');
	var address_error = document.getElementById('address_error');

	if( ! address)
	{
		address_error.innerHTML = 'Please enter address';
		address_obj.style.borderColor = 'red';
		return false;
	}
	else
	{
		address_error.innerHTML = '';
		address_obj.style.borderColor = 'green';
		return true;
	}
}

/**
*@name check_country
*@description This function is for checking country is selected or not and
*match country value is digit or not
*@param country
*@return boolean
*/
function check_country(country)
{
	country       = country.trim();
	country_obj   = document.getElementById('country');
	country_error = document.getElementById('country_error');

	if( ! country)
	{
		country_error.innerHTML = 'Please select country';
		country_obj.style.borderColor = 'red';
		return false;
	}
	else
	{
		var digits = /^[0-9]+$/;

		if( ! country.match(digits))
		{
			country_error.innerHTML = 'Please select country';
			country_obj.style.borderColor = 'red';
			return false;
		}
		else
		{
			country_error.innerHTML = '';
			country_obj.style.borderColor = 'green';
			return true;
		}
	}
}

/**
*@name check_country
*@description This function is for checking country is selected or not and
*match country value is digit or not
*@param country
*@return boolean
*/
function check_state(state)
{
	state       = state.trim();
	state_obj   = document.getElementById('state');
	state_error = document.getElementById('state_error');

	if( ! state)
	{
		state_error.innerHTML = 'Please select state';
		state_obj.style.borderColor = 'red';
		return false;
	}
	else
	{
		var digits = /^[0-9]+$/;

		if( ! state.match(digits))
		{
			state_error.innerHTML = 'Please select state';
			state_obj.style.borderColor = 'red';
			return false;
		}
		else
		{
			state_error.innerHTML = '';
			state_obj.style.borderColor = 'green';
			return true;
		}
	}
}

/**
*@name check_country
*@description This function is for checking country is selected or not and
*match country value is digit or not
*@param country
*@return boolean
*/
function check_city(city)
{
	city       = city.trim();
	city_obj   = document.getElementById('city');
	city_error = document.getElementById('city_error');

	if( ! city)
	{
		city_error.innerHTML = 'Please select city';
		city_obj.style.borderColor = 'red';
		return false;
	}
	else
	{
		var digits = /^[0-9]+$/;

		if( ! city.match(digits))
		{
			city_error.innerHTML = 'Please select city';
			city_obj.style.borderColor = 'red';
			return false;
		}
		else
		{
			city_error.innerHTML = '';
			city_obj.style.borderColor = 'green';
			return true;
		}
	}
}

/**
*@name check_zip_code
*@description This function is for checking zip code field is empty or not
*@param zip
*@return boolean
*/
function check_zip_code(zip)
{
	zip       = zip.trim();
	zip_obj   = document.getElementById('zip');
	zip_error = document.getElementById('zip_error');

	if( ! zip)
	{
		zip_error.innerHTML = 'Please enter zip code';
		zip_obj.style.borderColor = 'red';
		return false;
	}
	else
	{
		zip_error.innerHTML = '';
		zip_obj.style.borderColor = 'green';
		return true;
	}
}

/**
*@name validation
*@description This function is for validating all the field of registration form
*@return boolean
*/
function validation()
{
	var first_name   = document.getElementById('first_name').value;
	var middle_name  = document.getElementById('middle_name').value;
	var last_name    = document.getElementById('last_name').value;
	var email        = document.getElementById('email').value;
	var password     = document.getElementById('password').value;
	var cnf_password = document.getElementById('cnf_password').value;
	var phone        = document.getElementById('phone').value;
	var address      = document.getElementById('address').value;
	var country      = document.getElementById('country').value;
	var state        = document.getElementById('state').value;
	var city         = document.getElementById('city').value;
	var zip          = document.getElementById('zip').value;

	var first_name_error   = check_first_name(first_name);
	var middle_name_error  = check_middle_name(middle_name);
	var last_name_error    = check_last_name(last_name);
	var email_error        = check_email(email);
	var password_error     = check_password(password);
	var cnf_password_error = check_cnf_password(cnf_password);
	var phone_error        = check_phone(phone);
	var address_error      = check_address(address);
	var country_error      = check_country(country);
	var state_error        = check_state(state);
	var city_error         = check_city(city);
	var zip_error          = check_zip_code(zip);

	if(     first_name_error
		&&  middle_name_error
		&&  last_name_error
		&&  email_error
		&&  password_error
		&&  cnf_password_error
		&&  phone_error
		&&  address_error
		&&  country_error
		&&  state_error
		&&  city_error
		&&  zip_error
	  )
	{
		return true;
	}
	else
	{
		return false;
	}
}

/**
*@name number_only
*@description This function is for allow only nubers in a input field
*@param e event object
*@return boolean
*/
function number_only(e)
{
	try {
		var key;
		var keychar;

		if(window.event)
		{
			key = window.event.keyCode;
		}
		else if(e)
		{
			key = e.which;
		}
		else
		{
			return true;
		}

		keychar = String.fromCharCode(key);

		//Controls keys
		if((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
		{
			return true;
		}
		//Only numbers
		else if(('0123456789').indexOf(keychar) > -1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	catch(err) {
		alert(err.Descrition);
	}
}

/**
*@name text_only
*@description This function is for allow only text in a input field
*@param e event object
*@return boolean
*/
function text_only(e)
{
	try {

		var key;

		if(window.event)
		{
			key = window.event.keyCode;
		}
		else if(e)
		{
			key = e.which;
		}
		else
		{
			return true;
		}
		
		//Allow letters only
		if((key > 64 && key < 91) || (key > 96 && key < 123))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	catch(err) {
		alert(err.Descrition);
	}
}

/**
*@name first_position_space_prevent
*@description This function is for prevent space on first position of input field
*@param e event object, value value of the input field
*@return boolean
*/
function first_position_space_prevent(e, value)
{
	var len = value.trim().length;

	if(len == 0 && e.which === 32)
	{
		return false;
	}
	else
	{
		return true;
	}
}
