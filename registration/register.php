<?php
/**
*This is the all the html content for registration page and also for showing all server site validation error
*for the required field
*/ 
//starting session
session_start(); 

//including database connection file
require_once('connection.php');
//session_destroy();
?>
<!DOCTYPE html>
<html lang="en">
<!--head section start-->
<head>
	<title>Registartion Form</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link type="text/css" rel="stylesheet" href="../assets/css/style.css">
</head>
<!--head section end-->
<body>
	<div class="container">

		<?php
			require_once('../session-message.php');
		?>

	    <h1 class="well">Registration Form</h1>
		<div class="col-lg-12 well">
			<div class="row">
				<form method="post" action="reg-success.php" onsubmit="return validation()" novalidate="true" enctype="multipart/form-data">
					<div class="col-sm-12">
						<div class="row">
							<div class="col-sm-4 form-group">
								<label for="first_name">First Name <span class="required">*</span></label>
								<input type="text" placeholder="Enter First Name Here.." class="form-control" name="first_name" id="first_name" maxlength="20" value="<?php echo isset($_SESSION['post_data']['first_name']) ? $_SESSION['post_data']['first_name'] : ''; ?>" onkeypress="return text_only(event)" onblur="check_first_name(value)" required>
								<span class="error-msg" id="first_name_error"><?php echo ( ! empty($_SESSION['first_name_error'])) ? $_SESSION['first_name_error'] : ''; ?></span>
							</div>
							<div class="col-sm-4 form-group">
								<label for="middle_name">Middle Name</label>
								<input type="text" placeholder="Enter Middle Name Here.." class="form-control" name="middle_name" id="middle_name" maxlength="20" onkeypress="return text_only(event)">
								<span class="error-msg" id="middle_name_error"><?php echo ( ! empty($_SESSION['middle_name_error'])) ? $_SESSION['middle_name_error'] : ''; ?></span>
							</div>
							<div class="col-sm-4 form-group">
								<label for="last_name">Last Name</label>
								<input type="text" placeholder="Enter Last Name Here.." class="form-control" name="last_name" id="last_name" maxlength="20" onkeypress="return text_only(event)">
								<span class="error-msg" id="last_name_error"><?php echo ( ! empty($_SESSION['last_name_error'])) ? $_SESSION['last_name_error'] : ''; ?></span>
							</div>
						</div>
						<div class="form-group">
							<label for="email">Email Address <span class="required">*</span></label>
							<input type="email" placeholder="Enter Email Address Here.." class="form-control" name="email" id="email" maxlength="40" value="<?php echo isset($_SESSION['post_data']['email']) ? $_SESSION['post_data']['email'] : ''; ?>" onblur="check_email(value)" required>
							<span class="error-msg" id="email_error"><?php echo ( ! empty($_SESSION['email_error'])) ? $_SESSION['email_error'] : ''; ?></span>
						</div>
						<div class="form-group">
							<label for="password">Password <span class="required">*</span></label>
							<input type="password" placeholder="Enter Password Here.." name="password" id="password" class="form-control" maxlength="15" minlength="6" value="<?php echo isset($_SESSION['post_data']['password']) ? $_SESSION['post_data']['password'] : ''; ?>" onblur="check_password(value)" required>
							<span class="error-msg" id="password_error"><?php echo ( ! empty($_SESSION['password_error'])) ? $_SESSION['password_error'] : ''; ?></span>
						</div>
						<div class="form-group">
							<label for="cnf_password">Confirm Password <span class="required">*</span></label>
							<input type="password" placeholder="Enter Confirm Password Here.." name="cnf_password" id="cnf_password" class="form-control" maxlength="15" minlength="6" value="<?php echo isset($_SESSION['post_data']['cnf_password']) ? $_SESSION['post_data']['cnf_password'] : ''; ?>" onblur="check_cnf_password(value)" required>
							<span class="error-msg" id="cnf_password_error"><?php echo ( ! empty($_SESSION['cnf_password_error'])) ? $_SESSION['cnf_password_error'] : ''; ?></span>
						</div>
						<div class="form-group">
							<label for="phone">Phone Number <span class="required">*</span></label>
							<input type="text" placeholder="Enter Phone Number Here.." class="form-control" name="phone" maxlength="10" minlength="10" id="phone" value="<?php echo isset($_SESSION['post_data']['phone']) ? $_SESSION['post_data']['phone'] : ''; ?>" onkeypress="return number_only(event)" required onblur="check_phone(value)">
							<span class="error-msg" id="phone_error"><?php echo ( ! empty($_SESSION['phone_error'])) ? $_SESSION['phone_error'] : ''; ?></span>
						</div>
						<div class="form-group">
							<label for="male">Gender <span class="required">*</span></label>
							<input type="radio" name="gender" id="male" value="1" <?php echo ($_SESSION['post_data']['gender'] == 1) ? 'checked' : 'checked'; ?> ><label for="male">Male</label>
							<input type="radio" name="gender" id="female" value="2" <?php echo ($_SESSION['post_data']['gender'] == 2) ? 'checked' : ''; ?> ><label for="female">Female</label>
							<input type="radio" name="gender" id="other" value="3" <?php echo ($_SESSION['post_data']['gender'] == 3) ? 'checked' : ''; ?> ><label for="other">Other</label><br>
							<span class="error-msg" id="gender_error"><?php echo (! empty($_SESSION['gender_error'])) ? $_SESSION['gender_error'] : '';?></span>
						</div>					
						<div class="form-group">
							<label for="address">Address <span class="required">*</span></label>
							<textarea placeholder="Enter Address Here.." rows="3" class="form-control" id="address" maxlength="" name="address" onkeypress="return first_position_space_prevent(event, value)" onblur="check_address(value)">
								<?php echo isset($_SESSION['post_data']['address']) ? $_SESSION['post_data']['address'] : ''; ?>
							</textarea>
							<span class="error-msg" id="address_error"><?php echo ( ! empty($_SESSION['address_error'])) ? $_SESSION['address_error'] : ''; ?></span>
						</div>	
						<div class="row">
							<div class="col-sm-3 form-group">
								<label for="country">Country <span class="required">*</span></label>
								<select class="form-control" name="country" id="country" onblur="check_country(this.value)" onchange="state_list(this.value)">
									<option value="">select country</option>
									<?php
										$sql    = 'select id,name from countries';
										$result = $conn->query($sql);

										if($result->num_rows > 0)
										{
											//showing result of each row
											while($row = $result->fetch_assoc())
											{
									?>
												<option value="<?php echo $row['id']; ?>" <?php echo ($_SESSION['post_data']['country'] == $row['id']) ? 'selected' : ''; ?>><?php echo $row['name']; ?></option>
									<?php
											}
										}
										else
										{
											echo '<option value="">Country not available</option>';
										}
									?>
								</select>
								<span class="error-msg" id="country_error"><?php echo ( ! empty($_SESSION['country_error'])) ? $_SESSION['country_error'] : ''; ?></span>
							</div>
							<div class="col-sm-3 form-group">
								<label for="state">State <span class="required">*</span></label>
								<select class="form-control" id="state" name="state" onblur="check_state(this.value)" onchange="city_list(this.value)">
									<option value="">Select country first</option>
								</select>
								<span class="error-msg" id="state_error"><?php echo ( ! empty($_SESSION['state_error'])) ? $_SESSION['state_error'] : ''; ?></span>
							</div>	
							<div class="col-sm-3 form-group">
								<label for="city">City <span class="required">*</span></label>
								<select id="city" name="city" onblur="check_city(this.value)" class="form-control">
									<option value="">Select state first</option>
								</select>
								<span class="error-msg" id="city_error"><?php echo ( ! empty($_SESSION['city_error'])) ? $_SESSION['city_error'] : ''; ?></span>
							</div>	
							<div class="col-sm-3 form-group">
								<label for="zip">Zip Code<span class="required">*</span></label>
								<input type="text" placeholder="Enter Zip Code Here.." class="form-control" value="<?php echo isset($_SESSION['post_data']['zip']) ? $_SESSION['post_data']['zip'] : ''; ?>" onkeypress="return number_only(event)" maxlength="6" id="zip" name="zip" onblur="check_zip_code(value)">
								<span class="error-msg" id="zip_error"><?php echo ( ! empty($_SESSION['zip_error'])) ? $_SESSION['zip_error'] : ''; ?></span>
							</div>		
						</div>
						<div class="form-group">
							<label for="image">Profile Photo</label>
							<input type="file" class="form-control" name="image" id="image" accept=".jpg, .png, .jpeg">
							<?php
								if(! empty($_SESSION['image_error']))
								{
							?>
									<span class="error-msg"><?php echo $_SESSION['image_error']; ?></span>
							<?php
								}
							?>
						</div>									
						<button type="Submit" class="btn btn-lg btn-info" name="register">Register</button>					
					</div>
				</form> 
			</div>
		</div>
		<h3 class="well">Already have account? <a href="../index.php">Login Here</a></h3>
	</div>

	<!--including js files-->
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script type="text/javascript" src="../assets/js/common.js"></script>
</body>
</html>