<?php
//including database connection file
require_once('connection.php');

if( ! empty($_POST['country_id']))
{
	$sql = 'select id,name from states where country_id = "'.$_POST['country_id'].'" ORDER BY name ASC';
	$result = $conn->query($sql);

	if($result->num_rows > 0)
	{
		echo '<option value="">select state</option>';
		//fetching result of each row
		while($row = $result->fetch_assoc())
		{
			echo '<option value="'.$row['id'].'">'.$row['name'].'</option>';
		}
	}
	else
	{
		echo '<option value="">State not available</option>';
	}
}
elseif ( ! empty($_POST['state_id'])) 
{
	$sql = 'select id,name from cities where state_id = "'.$_POST['state_id'].'" ORDER BY name ASC';
	$result = $conn->query($sql);

	if($result->num_rows > 0)
	{
		echo '<option value="">select city</option>';
		//fetching result of each city row
		while($row = $result->fetch_assoc())
		{
			echo '<option value="'.$row['id'].'">'.$row['name'].'</option>';
		}
	}
	else
	{
		echo '<option value="">City not available</option>';
	}
}
else
{
	echo '<option value="">Internal server error</option>';
}

?>