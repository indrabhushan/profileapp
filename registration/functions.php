<?php
/**
*This file contain all the functions which is used in reg-success file;
*/


/**
*@name check_phone
*@description this function is for checking phone is empty or numeric value
*@param $phone submitted phone number
*@return $phone
*/
function check_phone($phone)
{
	//Assiging connection from global scope
	$conn = $GLOBALS['conn'];

	if(empty($phone))
	{
		$_SESSION['phone_error'] = 'Please enter phone number';
		return VALIDATION_ERROR;
	}
	else
	{
		$phone = test_input($phone);

		//checking enter phone number is numeric or not
		if( ! is_numeric($phone))
		{
			$_SESSION['phone_error'] = 'Please enter only numeric value';
			return VALIDATION_ERROR;
		}
		else
		{
			$sql    = 'select user_id from users where mobile = "'.$phone.'"';
	    	$result = $conn->query($sql);

	    	if($result->num_rows > 0)
	    	{
	    		$_SESSION['phone_error'] = 'Phone number already exist';
	    		return VALIDATION_ERROR;
	    	}
	    	else
	    	{
				//unset phone error key from session
				unset($_SESSION['phone_error']);
				return $conn->real_escape_string($phone);
			}
		}
	}
}

/**
*@name check_email
*@desc this function is for checking empty and valid email or not
*@param $email submitted email
*@return $email
*/
function check_email($email)
{
	//Assiging connection from global scope
	$conn = $GLOBALS['conn'];

	if(empty($email))
	{
		$_SESSION['email_error'] = 'Please enter email';
		return VALIDATION_ERROR;
	}
	else
	{
		$email = test_input($email);

		if ( ! filter_var($email, FILTER_VALIDATE_EMAIL)) 
	    {
	      $_SESSION['email_error'] = 'Please enter valid email address';
	      return VALIDATION_ERROR;
	    }
	    else
	    {
	    	$sql    = 'select user_id from users where email = "'.$email.'"';
	    	$result = $conn->query($sql);

	    	if($result->num_rows > 0)
	    	{
	    		$_SESSION['email_error'] = 'Email already exist';
	    		return VALIDATION_ERROR;
	    	}
	    	else
	    	{
	    		unset($_SESSION['email_error']);
	    		return $conn->real_escape_string($email);
	    	}
	    }
	}
}

/**
*@name check_empty_field
*@description this is common function for checking empty field or not
*@param $field_name which field you want to check, $message error message to show, $message_key which key want to set message
*@return $field_name value of that field
*/
function check_empty_field($field_name, $message, $message_key)
{
	//Assiging connection variable from global scope
	$conn = $GLOBALS['conn'];

	if(empty($field_name))
	{
		$_SESSION[$message_key] = $message;
		return VALIDATION_ERROR;
	}
	else
	{
		unset($_SESSION[$message_key]);
		return $conn->real_escape_string(test_input($field_name));
	}
}

/**
*@function test_input
*@description This function is for trimming extra white spaces and html special character
*@param $data form input data
*@return $data
*/
function test_input( $data )
{
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);

	return $data;
}

/**
*@name image_upload
*@description this function is for the uploading image
*@param $file_name, $tmp_name, $file_size, $image_type
*@return $file_name
*/
function image_upload($file_name, $tmp_name, $file_size, $image_type)
{
	//Assigning connection variable from global scope
	$conn = $GLOBALS['conn'];
	$target_dir = "../assets/images/";
	$target_file = $target_dir . basename($file_name);
	$upload_ok  = 1;
	
	// Check file size
	if ($file_size > 2000000) 
	{
	    $_SESSION['image_error'] = 'Only 2 mb image allowed';
	    $upload_ok = 0;
	}
	// Allow certain file formats
	/*if(    $image_type != 'image/jpg' 
		|| $image_type != 'image/png' 
		|| $image_type != 'image/jpeg'
	   ) 
	{
	    $_SESSION['image_error'] = 'Sorry, only JPG, JPEG, PNG files are allowed.';
	    $upload_ok = 0;
	}*/
	// Check if $upload_ok is set to 0 by an error
	if ($upload_ok == 0) 
	{
	    return VALIDATION_ERROR;
	} 
	else
	{
	    if (move_uploaded_file($tmp_name, $target_file)) 
	    {
	    	unset($_SESSION['image_error']);
	        return $conn->real_escape_string($file_name);
	    } 
	    else 
	    {
	    	$_SESSION['image_error'] = 'Sorry, there was an error uploading your file.';
	        return VALIDATION_ERROR;
	    }
	}
}

/**
*
*/
//including PHPMailer class
use PHPMailer\PHPMailer\PHPMailer;
require_once('../libraries/PHPMailer/src/PHPMailer.php');
require_once('../libraries/PHPMailer/src/SMTP.php');
require_once('../libraries/PHPMailer/src/Exception.php');
function send_email($email, $name)
{
	//Creating instace of PHPMailer
	$mail = new PHPMailer;
	$mail->isSMTP();
	$mail->SMTPDebug = 0;
	$mail->Host = 'smtp.gmail.com';
	$mail->Port = 587;

	//set type of protection
	$mail->SMTPSecure = 'tls';

	//set authentication to true
	$mail->SMTPAuth = true;

	//gmail login details
	$mail->Username = "yindrabhushan@gmail.com";
	$mail->Password = "8292179646";

	//set who is sending the email
	$mail->setFrom('yindrabhushan@gmail.com', 'Profileapp');

	//set where we are sending email
	$mail->addAddress($email, $name);

	//set subject of email
	$mail->Subject = 'Registration successfull';

	//set body of the email
	$mail->Body = 'Thank you for registring with us.';

	//check send email
	if( ! $mail->send()) 
	{
	    return FALSE;
	} 
	else 
	{
	    return TRUE;
	}
}

?>