<?php
/**
*This page is open when registartion form getting submit
*/
session_start();
//Including  form validation library
require_once('../libraries/Form_validation.php');
require_once('../libraries/Db_curd.php');
require_once('../libraries/gmail.php');

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
	//Create object of form validation
	$form_validation = new Form_validation;
	$db = new Db_curd;

	$first_name   = $form_validation->check_first_name($_POST['first_name']);
	$middle_name  = $form_validation->check_alphabet_only($_POST['middle_name'], 'middle_name_error', 'Please enter letters only');
	$last_name    = $form_validation->check_alphabet_only($_POST['last_name'], 'last_name_error', 'Please enter letters only');
	$email        = $form_validation->check_email($_POST['email']);
	$password     = $form_validation->check_password($_POST['password']);
	$cnf_password = $form_validation->check_cnf_password($password, $_POST['cnf_password']);
	$phone        = $form_validation->check_phone($_POST['phone']);
	$gender       = $form_validation->check_gender($_POST['gender']);
	$address      = $form_validation->check_address($_POST['address']);
	$country      = $form_validation->check_country_state($_POST['country'], 'country_error', 'Please select country');
	$state        = $form_validation->check_country_state($_POST['state'], 'state_error', 'Please select state');
	$city         = $form_validation->check_country_state($_POST['city'], 'city_error', 'Please select city');
	$zip          = $form_validation->check_zip_code($_POST['zip']);
	$image        = $form_validation->image_upload($_FILES['image']);

	$res = $first_name & $email & $password & $cnf_password & $phone & $gender & $address & $state & $country & $zip & $city;
	//Check validation error then redirect to register page
	if(($res && $middle_name && $last_name && $image) 
		|| ($res && empty($_POST['middle_name']) && empty($_POST['last_name']) && empty($_POST['image'])))
	{
		$data = array(
				'first_name'  => $first_name,
				'middle_name' => $middle_name,
				'last_name'   => $last_name,
				'gender'      => (int) $gender,
				'mobile'      => $phone,
				'email'       => $email,
				'password'    => password_hash($password, PASSWORD_BCRYPT),
				'address'     => $address,
				'country_id'  => (int) $country,
				'state_id'    => (int) $state,
				'city_id'     => (int) $city,
				'zip_code'    => $zip,
				'image'       => $image,
				'created_by'  => $first_name,
			);
		print_r($data);

		if($db->insert('users', $data))
		{
			// send email
			if(send_email($email, $first_name.' '.$middle_name.' '.$last_name))
			{
				unset($_SESSION['post_data']);
				$_SESSION['success_msg'] = 'Registration successfull, confirmation email is send to your email id';
				echo '<script>window.location = "../index.php";</script>';
			}
			else
			{
				unset($_SESSION['post_data']);
				$_SESSION['success_msg'] = 'Registration successfull, unable to send email';
				echo '<script>window.location = "../index.php";</script>';
			}
		}
		else
		{
			$_SESSION['post_data'] = $_POST;
			$_SESSION['error_msg'] = 'Internal server error! Please try again';
			echo '<script>window.location = "register.php"</script>';
		}
	}
	else
	{
		unset($_SESSION['error_msg']);
		unset($_SESSION['success_msg']);
		$_SESSION['post_data'] = $_POST;
		echo '<script>window.location = "register.php"</script>';
	}
}
?>