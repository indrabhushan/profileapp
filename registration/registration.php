<?php
/**
*This page is open when registartion form getting submit
*/
//starting session
session_start();

//Including database connection file
require_once('connection.php');

//Including all the function containing file
require_once('functions.php');

//defing validation error constant
define('VALIDATION_ERROR', 1);

//Checking server request is post method or something else
if($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$first_name   = check_empty_field($_POST['first_name'], 'Please enter first name', 'first_name_error');
	$middle_name  = $_POST['middle_name'];
	$last_name    = $_POST['last_name'];
	//check data to insert or update
	if(isset($_POST['register']))
	{
		$email        = check_email($_POST['email']);
		$password     = check_empty_field($_POST['password'], 'Please enter password', 'password_error');
		$cnf_password = check_empty_field($_POST['cnf_password'], 'Please enter confirm password', 'cnf_password_error');

		//checking if password and confirm password is same or not
		if($password != $cnf_password && $cnf_password != VALIDATION_ERROR)
		{
			$_SESSION['cnf_password_error'] = 'Please enter same password again';
			$cnf_password = VALIDATION_ERROR;
		}
	}
	//if update then these field has not require validation
	elseif(isset($_POST['update']))
	{
		$email        = 2;
		$password     = 2;
		$cnf_password = 2;
	}
	$phone        = check_phone($_POST['phone']);
	$gender       = check_empty_field($_POST['gender'], 'Please select gender', 'gender_error');
	$address      = check_empty_field($_POST['address'], 'Please enter address', 'address_error');
	$country      = check_empty_field($_POST['country'], 'Please select country', 'country_error');
	$state        = check_empty_field($_POST['state'], 'Please select state', 'state_error');
	$city         = ( ! empty($_POST['city'])) ? $_POST['city'] : 'NULL';
	$zip          = check_empty_field($_POST['zip'], 'Please enter zip code', 'zip_error');
	
	//check any field is empty
	if(    $first_name   === VALIDATION_ERROR 
		|| $email        === VALIDATION_ERROR 
		|| $password     === VALIDATION_ERROR
		|| $cnf_password === VALIDATION_ERROR
		|| $phone        === VALIDATION_ERROR
		|| $gender       === VALIDATION_ERROR
		|| $address      === VALIDATION_ERROR
		|| $country      === VALIDATION_ERROR
		|| $state        === VALIDATION_ERROR
		|| $zip          === VALIDATION_ERROR
	  )
	{
		if(isset($_POST['register']))
		{
			//setting all posted data
			$_SESSION['post_data'] = $_POST;
			//redirect to registration page
			echo '<script>window.location = "index.php"</script>';
		}
		elseif(isset($_POST['update']))
		{
			echo '<script>window.location = "../login/edit-profile.php"</script>';
		}
	}
	else
	{
		if(isset($_POST['register']))
		{
			$image    = image_upload($_FILES['image']['name'], $_FILES['image']['tmp_name'], $_FILES['image']['size'], $_FILES['image']['type']);
			$password = password_hash($password, PASSWORD_BCRYPT);
			$sql      = "INSERT INTO users(`first_name`, `middle_name`, `last_name`, `gender`, `mobile`, `email`, `password`, `address`, `country_id`, `state_id`, `city_id`, `zip_code`, `image`) 
			    VALUES('$first_name', '$middle_name', '$last_name', '$gender', '$phone', '$email', '$password', '$address', '$country', '$state', '$city', '$zip', '$image')";


			if($conn->query($sql))
			{
				// send email
				$sent = send_email($email, $first_name.' '.$middle_name.' '.$last_name);

				//Unsetting all the session error variable
				unset($_SESSION);
				//Detroy session
				//session_destroy();
				echo '<script>window.location = "reg-success.php";</script>';
			}
			else
			{
				//setting all posted data
				$_SESSION['post_data'] = $_POST;
				echo '<script>alert("Something wrong");</script>';
				//redirect to registration page
				echo '<script>window.location = "index.php";</script>'; 
			}
		}
		elseif(isset($_POST['update']))
		{
			if( ! empty($_FILES['image']['name']))
			{
				$image    = image_upload($_FILES['image']['name'], $_FILES['image']['tmp_name'], $_FILES['image']['size'], $_FILES['image']['type']);

				$image = image_upload($_FILES['image']);

				$sql      = "UPDATE users SET 
					`first_name` = '$first_name', 
					`middle_name` = '$middle_name', 
					`last_name` = '$last_name', 
					`gender` =  $gender,
					`mobile` = '$phone', 
					`address` = '$address', 
					`country_id` = $country, 
					`state_id` = $state, 
					`city_id` = $city, 
					`zip_code` = '$zip', 
					`image` = '$image' 
					WHERE user_id = ".$_SESSION['user_id']."";
			}
			else
			{
				$sql      = "UPDATE users SET 
				`first_name` = '$first_name', 
				`middle_name` = '$middle_name', 
				`last_name` = '$last_name', 
				`gender` = '$gender', 
				`mobile` = '$phone', 
				`address` = '$address', 
				`country_id` = $country, 
				`state_id` = $state, 
				`city_id` = $city, 
				`zip_code` = '$zip' 
				WHERE user_id = ".$_SESSION['user_id']."";
			}

			if($conn->query($sql))
			{
				$_SESSION['image'] = $image;
				echo '<script>alert("Update succesfully.");</script>';
				echo '<script>window.location = "../login/dashboard.php";</script>';
			}
			else
			{
				echo '<script>alert("Internal server error.");</script>';
				echo '<script>window.location = "../login/edit-profile.php";</script>';
			}
		}
	}
}

?>
