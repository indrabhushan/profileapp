<?php
/**
*This file is for the checking email already exit in database or not
*/
//Including database connection file
require_once('connection.php');
require_once('../libraries/constants.php');

//check ajax request

if(empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest')
{
	die('No direct script access allowed');
}

if( ! empty($_POST['email']))
{
	$message = [];
	$sql     = 'SELECT user_id FROM users WHERE email = "'.$_POST['email'].'"';
	$result  = $conn->query($sql);

	if($result->num_rows > 0)
	{
		$message['code']    = SUCCESS_CODE;
		$message['message'] = 'Email already exist';
	}
	else
	{
		$message['code']    = FAILED_CODE;
	}

	echo json_encode($message);
}
?>